# Add New UserId to the API database 

Add new user 'LoginId'

**URL** : `/api/user`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

username and password

**Data example:** All fields must be sent.

```json
{
    "Username":"test",
    "Password": "test1234"
}
```


## Success Response

**Condition** : if user is new or not, and it's login id 


**Code** : `200 OK`

**Content example** : 


```json
{
    "LoginId":"577a69a519c7",
    "IsNew": true
}
```

## Error Responses

**Condition** : If user is banned or something else happened

**Code** : `401 Unauthorized`

### OR

**Code** : `500 Internal Server Error`