# Check for new messages

Request to check for new updates ( new messages received )

**URL** : `/api/updates`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

an array of all current token and matchid pairs. 

**Data example:** All fields must be sent.

```json
[
	{
    "MatchId":"577a69a519c7ad1b4009f967578e91a786aaeb080ae670a1",
    "Token": "fe71235e-8658-4ea6-9eea-01e94ce865df"
	}
]
```


## Success Response

**Condition** : If there is new messages to those particular token and matchid pairs, you will receive messages in array of json objects. 

**IMPORTANT Note**: if ReceivedNumber is true, show a success green popup. 

**Code** : `200 OK`

**Content example**

```json
[
    {
        "Token": "fe71235e-8658-4ea6-9eea-01e94ce865df",
        "MatchId": "fe71235e-8658-4ea6-9eea-01e94ce865df",
        "ReceivedNumber": false
        "Messages": [
            {
                "_id": "57bee7979727703051158186",
                "match_id": "577a69a519c7ad1b4009f967578e91a786aaeb080ae670a1",
                "to": null,
                "from": "577a69a519c7ad1b4009f96",
                "message": "Hey",
                "timestamp": 1472128919519
            },
            {
                "_id": "57bee7979727703051158185",
                "match_id": "577a69a519c7ad1b4009f967578e91a786aaeb080ae670a2",
                "to": null,
                "from": "577a69a519c7ad1b4009f94",
                "message": "Hey there",
                "timestamp": 1472128919529
            }
        ]
    }
]
```

## Error Responses

**Condition** : If there is no new messages found now

**Code** : `200 OK`


**Content** : `[]`
