# RESTAPIDocs

These examples need to be revised carefully and implented when building the front-end as it shows how to work with the REST API

## Open Endpoints

#### All points reguires authentication named 'LoginId' that contains user ID*


## New User Id request

Manadatory request to register the 'LoginId' value that you will use for other requests

* [Add New Userid](user.md) : `POST /api/user`

## API requests

requests to use and what you need to have..etc
* [get stats of LoginId](stats.md) : `GET /api/user`
* [Get New Match](match.md) : `POST /api/match`
* [Send Message](message.md) : `POST /api/message`
* [Check for New Messages](updates.md) : `POST /api/updates`


## Live URL

here's a demo link for you to test with : http://217.23.15.139:21212/api/