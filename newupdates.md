# Url : http://chatnode-m7.herokuapp.com/
# credentails ( will be messaged )

# Features to add: 

## its not showing notifications of new messages. 
    > when a new message popup in a tab, it doesn't say anything. 

## messages aren't showing in a good format: https://i.imgur.com/w0WNlCJ.png
    > see screnshot i gave you. 
    > maybe only show first 2 words of message. 

## close chat option and stop checking for its updates
    > it should be next to profile a button named "close" in red. 

## ability to send message when clicking 'enter' and not have message be repeated twice. 
    > user shouldn't click "send", just Click "enter in his keyword and mesage will be sent.
    > message should be removed immediately to avoid double texting. 

## show date of when message got received.
    > See response in this : https://gitlab.com/neals/panel/blob/master/updates.md 
    > it has timestamps of messages. just convert them to hour and minute. and show it.

## automatically close tab after number got received and refresh the user stats.
    # See response in this : https://gitlab.com/neals/panel/blob/master/updates.md
    > there's a boolean argument named "ReceivedNumber", if true, close tab and automatically refresh user states. 

## there should be 2 shortcuts user can use to navigate: 
    > **Crtl+M** shortcut will be the equivalent of "add new"
    > **Crtl+A** shortcut will switch to the tab that has new messages. 
    
## links should be hyperlinked.

## show report option besides close option that has this request and close the tab: 

**URL** : `/api/report`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

report tab

**Data example:** All fields must be sent.

```json
{    
	"MatchId":"577a69a519c7ad1b4009f967578e91a786aaeb080ae670a1",    
	"Token": "fe71235e-8658-4ea6-9eea-01e94ce865df"
}
```

## show unmatch option besides close and report option
    >  it has the same request as report except instead of url /api/report, have /api/unmatch

