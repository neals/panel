# Get Stats of The UserId


**URL** : `/api/user`

**Method** : `GET`

**Auth required** : YES


## Success Response

**Condition** : If User exists

**Code** : `200 OK`

**Content example** : Example 
```json
{
    "Numbers": 10,
    "Chats": 75,
}
```

## Error Responses

**Condition** : If user doesn't exist

**Code** : `401 Unauthorized`

### OR

**Code** : `500 Internal Server Error`