# Request new match

Request new match

**URL** : `/api/match`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

neednew boolean set to true and how many matches account already have.

**Data example:** All fields must be sent.

```json
{
    "NeedNew":true,
    "HaveXMatches": 30
}
```


## Success Response

**Condition** : If there is new match, you will get one match object

**Code** : `200 OK`

**Content example**

```json
{
    "MyName": "Ronaldo",
    "MyAge": "21",
    "MyPics": [
        "url1",
        "url2"
    ],
    "MyBio": "Hey",
    "City": "Madrid",
    "YourName": "Elliot",
    "YourAge": "19",
    "YourPics": [
        "url1",
        "url2"
    ],
    "YourBio": "I like food!",
    "Id": 0,
    "MatchId": "577a69a519c7ad1b4009f967578e91a786aaeb080ae670a1",
    "Token": "fe71235e-8658-4ea6-9eea-01e94ce865df",
    "messages": [
        {
            "_id": "57bee7979727703051158186",
            "match_id": "577a69a519c7ad1b4009f967578e91a786aaeb080ae670a1",
            "to": null,
            "from": "577a69a519c7ad1b4009f96",
            "message": "Hey",
            "timestamp": 1472128919519
        },
        {
            "_id": "57bee7979727703051158185",
            "match_id": "577a69a519c7ad1b4009f967578e91a786aaeb080ae670a2",
            "to": null,
            "from": "577a69a519c7ad1b4009f94",
            "message": "how are you?",
            "timestamp": 1472128919529
        }
    ]
}
```

## Error Responses

**Condition** : If there is no match found now

**Code** : `200 OK`


**Content** : `no matches found, please try again in few seconds!`
