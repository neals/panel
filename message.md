# Send message to user

Send Message to a matchid

**URL** : `/api/message`

**Method** : `POST`

**Auth required** : YES

**Data constraints**

Matchid, token and message body 

**Data example:** All fields must be sent.

```json
{
    "ContentBody":"Hey there",
    "MatchId":"577a69a519c7ad1b4009f967578e91a786aaeb080ae670a1",
    "Token": "fe71235e-8658-4ea6-9eea-01e94ce865df"
}
```


## Success Response

**Condition** : If matchid and token exists, message will be sent

**Code** : `200 OK`

**Content example**

```text
"Message is being sent!"
```

## Error Responses

**Condition** : If anything is null or empty

**Code** : `400 BAD REQUEST`


**Content** : `empty token OR empty content body OR empty match id`
